import paho.mqtt.client as mqtt
import pvaccess
import sys
from lib import links
import time
import json


class Server(object):
    def __init__(self):
        connected = False
        while not connected:
            try:
                self.server = pvaccess.PvaServer()
            except RuntimeError as err:
                print(err)
                print('Retrying in 10s')
                time.sleep(10)
            else:
                connected = True
        self.linklist = links.LinkList()

    def epics_datatype(self, lnk):
        """ Using a dict to find corresponding datatype to be used in epics. Looking at the typ defined (mandatory) in
             the links secop_structure"""
        datatype = {
            'int': pvaccess.LONG,
            'double': pvaccess.DOUBLE,
            'string': pvaccess.STRING,
            'bool': pvaccess.INT,  # TODO: ShaLL be pvaccess.BOOLEAN here! BUT CS Studio cannot handle BOOLEAN currently. Issue added.
            'enum': pvaccess.INT
        }
        lnk.GW_datatype = datatype.get(lnk.secop_datatype.structure()["type"], None)
        if lnk.GW_datatype is None:
            #  TODO: What action if no secop datatype provided? Exit with error? Or as below, try to find old "datatype"
            #  try using the obsolete datatype (from the old links object), if not use string..
            lnk.GW_datatype = datatype.get(lnk.as_dict()["datatype"], None)
            if lnk.GW_datatype is None:
                lnk.GW_datatype = pvaccess.STRING
                #  Need to add proper handling
                print('No epics datatype, defaults to str....')
        #print("The link python datatype:  {} ".format(lnk.python_datatype))
        return

    def python_datatype(self, lnk):
        """ Using a dict to find corresponding datatype to be used in python. Looking at the typ defined (mandatory) in
             the links secop_structure"""
        datatype = {
            'int': int,
            'double': float,
            'string': str,
            'bool': bool
        }
        lnk.python_datatype = datatype.get(lnk.secop_datatype.structure()["type"], None)
        #print("The link python datatype:  {} ".format(lnk.python_datatype))
        if not lnk.python_datatype:
            print('No python datatype, defaults to str')
            lnk.python_datatype = str
        return

    def add_pv(self, lnks, write_to_topic_func):
        """
        Creates a PvObject for get_reference and set_reference if defined in the link. 1 PV is configured for each.
        The PV objects are then added to the pvaccess server as a record with a PV name. The object is also added to
        the link and can be accessed for update.
        :param lnks:
        :param write_to_topic_func:
        :return:
        """
        for lnk in lnks:
            # Passing the link object to functions adding (to the link) information on datatype
            self.epics_datatype(lnk)
            self.python_datatype(lnk)
            ts = pvaccess.PvTimeStamp(1, 1)  # Creating a pvapy timestamp structure to define the type in the PVobject
            al = pvaccess.PvAlarm(0, 0, "")  # Creating a pvapy alarm structure to define the type in the PVobject
            try:
                get_pv = pvaccess.PvObject({'value': lnk.GW_datatype,
                                            'pvname': pvaccess.STRING,
                                            'timeStamp': ts,
                                            'alarm': al,
                                            'descriptor': pvaccess.STRING,
                                            'unit': pvaccess.STRING,
                                            'lo': pvaccess.FLOAT,
                                            'lolo': pvaccess.FLOAT,
                                            'hi': pvaccess.FLOAT,
                                            'hihi': pvaccess.FLOAT})

            except ValueError as err:
                print(err)
                return
            #  Adding the created pvObject to the Link object (this allows updating in callback)
            lnk.get_reference_pv_obj = get_pv
            if lnk.get_reference:
                #  Adding the getPV as a record, setting the PV name from link(get_reference)
                #  Should a get pv be used for "triggering" a read of the get reference(?...) Not implemented currently
                self.server.addRecord(lnk.get_reference, get_pv)
                # Add static data. TODO: Below shoudl be adapted to use the data in secop_structure!
                if hasattr(lnk, 'descriptor'):
                    get_pv.setString('descriptor', str(lnk.description))
                if hasattr(lnk, 'unit'):
                    get_pv.setString('unit', str(lnk.unit))
                if hasattr(lnk, 'min'):
                    get_pv.setFloat('lo', float(lnk.min))
                if hasattr(lnk, 'max'):
                    get_pv.setFloat('hi', float(lnk.max))

            else:
                print("There is no get reference")

            if lnk.set_reference:
                #  Creating a new PV object for the set_reference
                set_pv = pvaccess.PvObject({'value': lnk.GW_datatype,
                                            "pvname": pvaccess.STRING,
                                            'timeStamp': ts,
                                            'alarm': al,
                                            'descriptor': pvaccess.STRING})
                # Populating the name field with link data, needed to get the correct link later
                set_pv.set({"pvname": lnk.set_reference})  #  TODO: verify if this is obsolete..
                #  Adding the created pvObject to the Link object TODO (almost making the pvname above redundant?
                lnk.set_reference_pv_obj = set_pv
                #  Adding the created PV as record to the server. Passing the function allowing publishing data on
                #  set topic. Triggers on channel write.
                self.server.addRecord(lnk.set_reference, set_pv, write_to_topic_func)
            else:
                print("No set reference on this link")

            if lnk.set_reference:
                print("Adding PV: set reference ->  {}  set topic->  {}".format(lnk.set_reference, lnk.set_topic))
                print("Adding PV: get reference ->  {}  get topic->  {}".format(lnk.get_reference, lnk.get_topic))
            else:
                print("Adding PV: get reference ->  {}  get topic->  {}".format(lnk.get_reference, lnk.get_topic))

        return lnks  # Returns a link list of all listed links on this "server"

    def remove_pv(self, lnk):
        # self.server.removeRecord(lnk.set_reference)
        if lnk.set_reference:
            if self.server.hasRecord(lnk.set_reference):
                print("removing set ref PV")
                self.server.removeRecord(lnk.set_reference)
        if lnk.get_reference:
            if self.server.hasRecord(lnk.get_reference):
                print("removing get ref PV")
                self.server.removeRecord(lnk.get_reference)

    def write_to_pv(self, client, userdata, msg):
        """
            This function is triggered as callback function when a link topic (e.g. from HW side)have been updated.
            The corresponding PV is fetched from the linklist by the messages value_topic (that was updated).
            The data in the msg should follow SECoP datainfo format [data, {"t":timestamp}, ....]

            TODO: Need to add syntax checks and also handling of more types.
            """
        message = json.loads(msg.payload.decode())
        #  Find the corresponding link from the topic in MQTT payload.
        #  Extracting the correct link using the "topic" as "key"
        lnk = self.linklist.find_by_value_topic(msg.topic)
        #  Transfer timestamp. Just a non verified dummy conversion used now..
        timestamp = pvaccess.PvTimeStamp(int(message[1]["t"]), int((message[1]["t"] % 1) * 1000000000))
        msg_data = None
        if isinstance(message[0], dict):
            #  Need to catch enum etc. Implement SECoP syntax. Probably requires better approach (for structs etc)
            if "index" in message[0]:
                msg_data = lnk.python_datatype(message[0].get("index"))
        else:
            msg_data = lnk.python_datatype(message[0])  # Extracting the data from the MQTT payload (and typecast)

        # Using the PVobject extracted from link
        pv = lnk.get_reference_pv_obj
        messagedict = message[1]
        if "alarm" in messagedict.keys():  # Just for testing ..
            print("Print out Alarm in PV structure:   {}    {}".format(message[1],messagedict["alarm"] ))

        #  Check link datatype to use correct pvapy setfunction. Write data to pv object
        #  Currently only int, double, string handled. ENUM etc need to be adressed

        link_datatype = lnk.secop_datatype.structure()["type"]
        if link_datatype is not None:

            if link_datatype == "double":
                pv.setDouble('value', msg_data)
            elif link_datatype == "string":
                pv.setString('value', msg_data)
            elif link_datatype == "int":
                #TODO: The typecast is not the way to go either (teh link has an datatype set, but the data is string..)
                pv.setInt('value', msg_data)
            elif link_datatype == "enum":
                #  TODO: make better solution using structures
                pv.setInt('value', int(msg_data))

            elif link_datatype == "bool": #TODO: Below is temporar fix as CSS cannot handle BOOLEAN.
                #pv.setBoolean('value', msg_data)
                val = None
                if msg_data == True:
                    val = 1
                elif msg_data == False:
                    val=0
                else:
                    print("something not working")
                pv.setInt('value', val)

            else:
                print ("Something went wrong when extracting datatype in writing to pv ")
        else:
            pv.setString('value', msg_data)
            print("Setting to string by default. something is wrong..)")

        #  Updates PV object with the timestamp data from message
        pv.setStructure('timeStamp', {'secondsPastEpoch': timestamp.getSecondsPastEpoch(),
                                      'nanoseconds': timestamp.getNanoseconds(),
                                      'userTag': timestamp.getUserTag()})
        if "alarm" in messagedict.keys():
            pv.setStructure('alarm', {'message': messagedict["alarm"] })

        #  Update the PV on teh server with the pv object
        try:
            self.server.update(lnk.get_reference, pv)
        except (ValueError) as err:
        # except (ValueError, pvaccess.ObjectNotFound) as err:
            print('internal, write_to_pv: ' + str(err))

        print("Recieved:  {}     on topic:  {}     writing to PV:   {}".format(message, msg.topic, lnk.get_reference))


class LinkClient(object):
    """
    A MQTT client setup to listen to topics for adding (or removing) a new ECS EPICS link.
    The callback function triggered by publishing on the "/add-link" expects a list (?) of links in dict format as a Json.
    """
    def __init__(self, topic=links.EPICS_ECS_GATEWAY_TOPIC, mqtt_host=links.OCTOPUS_DEFAULT_BROKER,
                 mqtt_port=links.DEFAULT_MQTT_TCP_PORT):
        self.topic = topic
        self.description = ""
        self.links = links.LinkList()
        self.mqtt_host = mqtt_host
        self.mqtt_port = mqtt_port
        self.mqtt_client = mqtt.Client()
        auth = {'username': links.OCTOPUS_DEFAULT_USER, 'password': links.OCTOPUS_DEFAULT_PASSWORD}
        if auth:
            self.mqtt_client.username_pw_set(**auth)
        self.server = Server()

        try:
            self.mqtt_client.on_connect = self.on_mqtt_connect
            self.mqtt_client.connect(self.mqtt_host, self.mqtt_port, 60)
            self.loops = 0
            self.mqtt_client.loop_start()  # Starts separate loop for handling MQTT calls
            while True:
                time.sleep(0.01)
                #  TODO: Align with other approaches. The iot_hwgw_gui_control.py uses the automatic .loop_start().
                if self.loops > 200:
                    # self.mqtt_client.publish('wd/' + self.topic, 'EPICS HWGW')  # TODO: for testing ink, fix!
                    self.expose_links_on_mqtt()  # For constant Update of GUI  TODO: Should this be here..
                    self.loops = 0
                else:
                    self.loops += 1

        except RuntimeError:
            self.cleanup('Runtime error')

        except KeyboardInterrupt:
            self.cleanup('Keyboard interrupt')

        self.cleanup('Normal')

    def cleanup(self, reason):
        print('Exiting: ' + reason)
        #self.mqtt_client.publish(self.topic + '/links', '', retain=False)  # not sure
        self.mqtt_client.loop_stop()
        self.mqtt_client.disconnect()
        sys.exit(0)

    def on_mqtt_connect(self, client, userdata, flags, rc):
        print("Connected with result code " + str(rc))
        self.mqtt_client.subscribe('#')
        #self.mqtt_client.message_callback_add(self.topic + "/add_link", self.on_mqtt_add_link_message)
        self.mqtt_client.message_callback_add(self.topic + '/remove_link', self.on_mqtt_remove_link_message)
        self.mqtt_client.message_callback_add("ses/structure/#", self.on_mqtt_add_describe_message)


    # def on_mqtt_add_link_message(self, client, userdata, msg):
    #     """OBSOLETE This function should be superseeded by the on below (using describe msg)
    #         Callback used for /add_link topic message should be a array of links in dict format as a Json(?)
    #         Checks if link already exists. IF so removes that and creates a new, adding it to the servers link list.
    #     """
    #     message = msg.payload.decode()
    #
    #     print('adding link')
    #     try:
    #         link_list_message = json.loads(message)
    #     except ValueError as err:
    #         print('received a non-valid json' + str(err))
    #         return
    #
    #     if "equipment_id" in link_list_message:
    #         link_list_message = links.make_ecs_link_list(link_list_message)
    #
    #     if not isinstance(link_list_message, list):
    #         print('links to add has to be a json-array')
    #         return
    #
    #     #  TODO: Need to add a check if the link already exsits ...but hmmm...also how to handle created epics PVs...
    #     #  AND need to remove/create PV if name changed or not, AND just change other data.
    #     #  TODO: Use "create_pv_and_mqtt_callbacks" function instead of section below .
    #     for link in link_list_message:
    #         print(link)
    #         if self.server.linklist.find_by_value_topic(link["value_topic"]):
    #             #  If link exists. Remove it/clean up. Then make a new one from scratch.
    #             self.remove_link(link["value_topic"])
    #             print("Removing link with value_topic: {}  , Creating new one now.".format(link["value_topic"]))
    #         else:
    #             print("New value topic.   {}".format(link["value_topic"]))
    #
    #     try:
    #         links_returned = self.server.linklist.add_links_from_list(link_list_message, colon=True)
    #     except AttributeError as error:
    #         print("Error adding link to linklist")
    #
    #     #  Add PVs for the link. Passing the function(self.on_pv_write_msg) for writing to MQTT set_topics as argument.
    #     lnks = self.server.add_pv(links_returned, self.write_to_topic)
    #
    #     for lnk in lnks:
    #         print("Link value topic: {}".format(lnk.value_topic))
    #         #  Adding callback function for updates on connected  MQTT topic. Topic updates triggers the MQTT
    #         #  callback function (in this case the write_to_pv funtion in the (PV) server), which updates the
    #         #  connected PV.
    #         self.mqtt_client.message_callback_add(lnk.value_topic, self.server.write_to_pv)
    #
    #     self.expose_links_on_mqtt()
    #
    #     return

    def on_mqtt_add_describe_message(self, client, userdata, pv_msg):
        """Adding ECS links from complete describe message (structure report). As a describe message is recieved on the
             dedicated topic, it sent to links lib to generate a list of links. For each link a connection is made
             allowing monitor/subscribe on epics and mqtt sides"""
        describe_msg = pv_msg.payload.decode()
        describe = json.loads(describe_msg)
        if describe["equipment_id"] == self.description: # Only one shall pass...for now...
            print("already running that secnode")
            return
        else:
            self.description = describe["equipment_id"]
            describe = links.rename_key("links", "ECS_link", describe)  #  TODO: make unified link approach
            validated_describe = links.validate_structure(describe)
            ecs_linklist = links.make_ecs_link_list(validated_describe)
            # Use topics for EPICS PV names. Requires replacing slash with colons. Also add set_reference, from topic
            for lnk in ecs_linklist:
                lnk.colonize()  # Exchanging / with : for PV names
            #  Adding all links to the servers linklist
            self.server.linklist = ecs_linklist
            self.create_pv_and_mqtt_callbacks(self.server.linklist)
            self.expose_links_on_mqtt()
            return

    def create_pv_and_mqtt_callbacks(self, ecs_linklist_dict):
        """ This function takes a list of ecs links as a dict.
         Each link in the list is check for duplicates (based on "value_topic") on server, any duplicates are removed.
         New links are added to server list
         PVs are created in the "add_link" server function
         Lastly MQTT callback functions are attached to the links to update PV when a MQTT topic is updated
         """
        #  TODO below is removed as currently all links are created in links.make_ecs_link_list, this part would then
        #   remove those, and recreate them via "Link" However that path currently do not ue the datainfo structure...
        # for link in ecs_linklist_dict:
        #     if self.server.linklist.find_by_value_topic(link["value_topic"]):
        #         #  If link exists. Remove it/clean up. Then make a new one from scratch.
        #         self.remove_link(link["value_topic"])
        #         # print("Removing link with value_topic: {}  , Creating new one now.".format(link["value_topic"]))
        #     else:
        #         pass
        #         # print("New value topic.   {}".format(link["value_topic"]))
        # try:
        #     links_returned = self.server.linklist.add_links_from_list(ecs_linklist_dict, colon=True)
        # except AttributeError as error:
        #     print("Error adding link to linklist")

        #  Add PVs for the link. Passing the function(self.on_pv_write_msg) for writing to MQTT set_topics as argument.
        # lnks = self.server.add_pv(links_returned, self.write_to_topic)


        #  Add PVs for links
        lnks = self.server.add_pv(ecs_linklist_dict, self.write_to_topic)

        for lnk in lnks:
            print("Link value topic: {}".format(lnk.value_topic))
            #  Adding callback function for updates on connected  MQTT topic. Topic updates triggers the MQTT
            #  callback function (in this case the write_to_pv funtion in the (PV) server), which updates the
            #  connected PV.
            self.mqtt_client.message_callback_add(lnk.value_topic, self.server.write_to_pv)
        self.print_summary()

    def print_summary(self):
        for link in self.links:
            print("Created GetPV: {}  linked to topic:   {}".format(link.get_reference, link.get_topic))
            print("Created SetPV: {}  linked to topic:   {}".format(link.set_reference, link.set_topic))


    def write_to_topic(self, pv_msg):
        """Function used by Server class to publish updated PV data on correct MQTT topic defined in corresponding link.
        This function is added as argument to when adding pv- Retrieves correct link from linklist using "pvname"
        """

        lnk = self.server.linklist.find_by_set_reference(pv_msg["pvname"])

        """ The construction below is used to temporarily enable use of BOOLEANS in CS Studio which is currently not 
        possible. When using a BOOLEAN  CS Studio interprets it as a large structure/string. The main "value" is not 
         possible to acces , but it is possible to acces e.g. description or unit from PV/unit etc"""
        val = pv_msg['value']

        if "datatype" in (lnk.as_dict()).keys():
            if "bool" == lnk.as_dict()['datatype']:
                if pv_msg["value"] == 1:
                    val= True
                elif pv_msg["value"] == 0:
                    val= False
                else:
                    print("something wrong with the bool fix")
        # print("In write_to_topic. Recieved PV msg:  " + pv_msg["pvname"] + "  will be published on topic:  " +
        #      lnk.set_topic + "  with value:  " + str(pv_msg["value"]))
        self.mqtt_client.publish(lnk.set_topic, json.dumps([val]).encode("utf-8"))

    def expose_links_on_mqtt(self):
        """
        Exposes (publishes) a list of available links on a topic available to a GUI
        :return:
        """

        self.mqtt_client.publish(self.topic + '/links', self.server.linklist.linklist_as_json(), retain=False)
        #  TODO: Test to expose links to the epics_ecsgw_controll. NEED new define path in "links" !!
        self.mqtt_client.publish("ecs_links", self.server.linklist.linklist_as_json(), retain=False)

    def remove_link(self, value_topic):
        lnk = self.server.linklist.find_by_value_topic(value_topic)
        if lnk:
            if lnk.set_topic:
                self.mqtt_client.message_callback_remove(lnk.set_topic)
            if lnk.get_topic:
                self.mqtt_client.message_callback_remove(lnk.get_topic)
            if lnk.value_topic:
                self.mqtt_client.message_callback_remove(lnk.value_topic)

            self.server.remove_pv(lnk)
            self.server.linklist.remove(lnk)

    def on_mqtt_remove_link_message(self, client, userdata, msg):
        self.remove_link(msg.payload.decode('utf-8'))
        self.expose_links_on_mqtt()


if __name__ == '__main__':
    lc = LinkClient()
    #  lc = LinkClient(topic="epics", mqtt_host="10.4.0.107")

