import sys
from os import path
import json
sys.path.insert(0, path.abspath(path.join(path.dirname(__file__), '..')))
import lib.secop_errors as secop_errors
import lib.secop_datainfo as datainfo

OCTOPUS_DEFAULT_BROKER = '10.0.0.19'
OCTOPUS_DEFAULT_USER = 'mesi'
OCTOPUS_DEFAULT_PASSWORD = 'vaha23neca'
# Predefined topics
DEFAULT_SECOP_TCP_PORT = 10767
DEFAULT_MQTT_TCP_PORT = 1883
OCTOPY_MAIN_TOPIC = 'octopy'
EPICS_ECS_GATEWAY_TOPIC = 'ecs/gw/epics'
SECOP_ECS_GATEWAY_TOPIC = 'ecs/gw/secop'
SOCKET_HW_GATEWAY_TOPIC = 'octopy/datasource/hwgw/socket'
GATEWAY_MANAGER_TOPIC = 'octopy/datasource'
HW_GATEWAY_TOPIC = 'octopy/datasource/hwgw'
EPICS_HW_GATEWAY_TOPIC = 'hw/gw/epics'
MQTT_HW_GATEWAY_TOPIC = 'hw/gw/mqtt'
HWGW_CONTROLLER_TOPIC = 'octopus/hwgw'
DATA_SOURCE_MANAGER_TOPIC = 'octopy/datasource'
OCTOPY_IOT_DATA_TOPIC = 'opctopy/data'
STRUCTURE_EDITOR_TOPIC = 'structure/edit'
""" The structure report editor listens to this topics, subtopic should be /<id>"""
GW_AVAILABLE_LINKS_TOPIC = 'links'
AVAILABLE_CHANNELS_TOPIC = DATA_SOURCE_MANAGER_TOPIC + '/channels'
""" This is a general topic where links are published, subtopic should be:
    /<hw|ecs>/gw/<socket|usb|serial|epics|...>/<<id>|<port>
    
    Subtopics /<id>/<version_number> might later be added"""
STRUCTURE_REPORTS_TOPIC = 'ses/structure'
""" This is a general topic where structure reports a published, subtopic should be /<id>.
    Subtopics /<id>/<version_number> might later be added"""
ECS_LINK_NAME = 'ECS_link'
HW_LINK_NAME = 'HW_link'

TABLES_STATE_MACHINE_TOPIC = 'int/sm/tables'
MANDATORY_NODE_KEYS = ('equipment_id', 'description')
OPTIONAL_NODE_KEYS = ('firmware', 'implementor', 'timeout')
MANDATORY_MODULE_KEYS = ('description', 'interface_classes')
OPTIONAL_MODULE_KEYS = ('visibility', 'description')
MANDATORY_ACCESSIBLE_KEYS = ('description', 'datainfo', 'readonly')
OPTIONAL_ACCESSIBLE_KEYS = ('equipment_id', 'description')
INTERNAL_LINK_STRING = 'INTERNAL'
INTERNAL_GET_TOPIC = 'should_be_internal'

EPICS_PV_NAME_SEPARATOR = ":"

GCO_TYPE_INTERNAL = 0
GCO_TYPE_MQTT = 1
DEFAULT_GCO = {'type': GCO_TYPE_MQTT}


def to_be_deprecated(text_string):
    print('WARNING THIS WILL BE DEPRECATED: {}'.format(text_string))


def rename_key(old_key, new_key, structure_as_dict):
    ret = dict()
    for k, v in structure_as_dict.items():
        k = k if not k == old_key else new_key
        if isinstance(v, dict):
            ret[k] = rename_key(old_key, new_key, v)
        else:
            ret[k] = v
    return ret


def validate_structure(input_structure):
    """ The structure report used on octopus has more information than the structure report defined by SECoP has.
    This function takes an extended structure from Octopus and returns a structure without octopus specific information
    like links"""

    if not isinstance(input_structure, dict):
        raise TypeError('structure should be a dict')

    validated_structure = dict()
    validated_structure['modules'] = dict()
    if input_structure.get('modules'):
        if not isinstance(input_structure['modules'], dict):
            raise TypeError('modules should be stored as a dict')
    else:
        raise secop_errors.SecopException('modules key missing')
    for node_key in MANDATORY_NODE_KEYS:
        try:
            validated_structure[node_key] = input_structure[node_key]
        except KeyError as err2_info:
            raise secop_errors.SecopException('Missing mandatory key/value-pair: {} at node level'.format(err2_info))
    for node_key in OPTIONAL_NODE_KEYS:
        try:
            validated_structure[node_key] = input_structure[node_key]
        except KeyError:
            pass
    for module_name in input_structure.get('modules'):
        current_module = input_structure['modules'].get(module_name)
        new_module = dict()
        new_module['accessibles'] = dict()
        for module_key in MANDATORY_MODULE_KEYS:
            if current_module.get(module_key):
                new_module[module_key] = current_module[module_key]
            else:
                if module_key == 'interface_classes':
                    new_module['interface_classes'] = ['readable']
                else:
                    raise secop_errors.SecopException('Missing mandatory key/value-pair: {} in module {}'
                                                      .format(module_key, module_key))
        for module_key in current_module.keys():
            if module_key not in new_module.keys():
                new_module[module_key] = current_module[module_key]
# ________________________ACCESSIBLES_____________________
        for accessible_name in current_module.get('accessibles'):
            current_accessible = current_module['accessibles'].get(accessible_name)
            new_accessible = dict()
            for accessible_key in MANDATORY_ACCESSIBLE_KEYS:
                if accessible_key == 'readonly' and accessible_key not in current_accessible.keys():
                    readonly = True
                    hw_link = current_accessible.get(HW_LINK_NAME)
                    if hw_link:
                        if hw_link.get('set_topic'):
                            readonly = False
                    ecs_link = current_accessible.get(ECS_LINK_NAME)
                    if ecs_link:
                        if ecs_link.get('set_topic'):
                            readonly = False
                    current_accessible[accessible_key] = readonly
                    continue
                if current_accessible.get(accessible_key) is not None:
                    new_accessible[accessible_key] = current_accessible[accessible_key]
                else:
                    raise secop_errors.SecopException('Missing mandatory key/value pair {} in {}:{}'
                                                      .format(accessible_key, module_name, accessible_name))
            for accessible_key in current_accessible.keys():
                if accessible_key not in new_accessible.keys():
                    if accessible_key in (HW_LINK_NAME, ECS_LINK_NAME):
                        lnk = Link(**current_accessible.get(accessible_key))
                        new_accessible[accessible_key] = lnk.structure()
                    else:
                        try:
                            new_accessible[accessible_key] = current_accessible[accessible_key]
                        except KeyError:
                            pass
            try:
                new_module['accessibles'][accessible_name] = new_accessible
            except KeyError:
                pass
        validated_structure['modules'][module_name] = new_module
    return validated_structure




""" Functions for extracting links from an extended structure"""
def make_hardware_link_list(structure, remove=False):
    """ Function used to extract link-lists for the hardware side"""
#    structure = validate_structure(structure)
    link_dict = {}
    for module in structure['modules'].keys():
        lnk_lst = None
        if module != '_order':
            link_list = LinkList()
            for accessible in structure['modules'][module]['accessibles']:
                if accessible != '_order':
                    if HW_LINK_NAME in structure['modules'][module]['accessibles'][accessible].keys():
                        try:
                            lnk_lst = Link(**structure['modules'][module]['accessibles'][accessible][HW_LINK_NAME])
                        except TypeError as err4_info:
                            print(err4_info)
                        if remove:
                            del(structure['modules'][module]['accessibles'][accessible][HW_LINK_NAME])
                    elif structure['modules'][module]['accessibles'][accessible]["datainfo"]["type"] == "command":
                        print("{}:{} is a command and should have a HWGW".format(module, accessible))
                    else:

                        raise KeyError('Accessible "{}" in module "{}" does not have a {}-key'.format(accessible,
                                                                                                      module,
                                                                                                      HW_LINK_NAME))
                    if lnk_lst:
                        link_list.update(lnk_lst)
            link_dict[module] = link_list
    return link_dict


def make_ecs_link_list(structure, remove=False):
    """ Function used to extract link-lists from structure reports for the ECS side.
     It first looks for an ECS-link, if no ECS-link exists for that accessible it creates one from the HW-link"""
#    structure = validate_structure(structure)
    link_list = LinkList()
    if not isinstance(structure['modules'], dict):
        raise AttributeError('Modules has to be dictionaries')
    for module in structure['modules'].keys():
        for accessible in structure['modules'][module]['accessibles']:
            if ECS_LINK_NAME in structure['modules'][module]['accessibles'][accessible].keys():
                sdt = structure['modules'][module]['accessibles'][accessible].get('datainfo')
                if not structure['modules'][module]['accessibles'][accessible][ECS_LINK_NAME].get('get_reference'):
                    ref = str(structure.get('equipment_id'))
                    if not ref:
                        raise secop_errors.BadJSON('The "equipment_id" key is missing')
                    ref += EPICS_PV_NAME_SEPARATOR + module + EPICS_PV_NAME_SEPARATOR + accessible
                    sr = None
                    readonly = structure['modules'][module]['accessibles'][accessible].get('readonly')
                    if readonly is not None:
                        sr = ref + ':set'
                    else:
                        secop_errors.ProtocolErrorError('readable not set: {} in module {}'.format(accessible, module))
                if not structure['modules'][module]['accessibles'][accessible][ECS_LINK_NAME].get('value_topic'):
                    try:
                        structure['modules'][module]['accessibles'][accessible][ECS_LINK_NAME]['value_topic'] =\
                            structure['modules'][module]['accessibles'][accessible][HW_LINK_NAME].get('value_topic')
                    except KeyError as hw_link_err:
                        print('Keyerror on {}:{} error:{}'.format(module, accessible, hw_link_err))
                lnk_lst = Link(ecs_link=True, secop_datatype=datainfo.from_structure(sdt),
                               **structure['modules'][module]['accessibles'][accessible][ECS_LINK_NAME])
                if remove:
                    del(structure['modules'][module]['accessibles'][accessible][ECS_LINK_NAME])
            elif HW_LINK_NAME in structure['modules'][module]['accessibles'][accessible].keys():
                val_t = structure['modules'][module]['accessibles'][accessible][HW_LINK_NAME].get('value_topic')
                set_t = structure['modules'][module]['accessibles'][accessible][HW_LINK_NAME].get('value_topic')
                sdt = structure['modules'][module]['accessibles'][accessible].get('datainfo')
                ref = structure.get('equipment_id')
                if not ref:
                    raise secop_errors.BadJSON('The "equipment_id" key is missing')
                ref += EPICS_PV_NAME_SEPARATOR + module + EPICS_PV_NAME_SEPARATOR + accessible
                sr = None
                if set_t:
                    sr = ref + ':set'
                lnk_lst = Link(ecs_link=True, value_topic=val_t, set_topic=set_t, get_reference=ref, set_reference=sr,
                               secop_datatype=datainfo.from_structure(sdt))
            else:
                raise KeyError('Accessible "{}" in module "{}" does not have a {} or {} -key'
                               .format(accessible, module, ECS_LINK_NAME, HW_LINK_NAME))
            lnk_lst.module = module
            lnk_lst.accessible = accessible
            link_list.update(lnk_lst)
    return link_list


def clean_describe(extended_structure):
    """ The structure report used on octopus has more information than the structure report defined by SECoP has.
    This function takes an extended structure from Octopus and returns a structure without octopus specific information
    like links"""
    if not isinstance(extended_structure, dict):
        raise TypeError('structure should be a dict')
    plain_structure = dict()
    plain_structure['modules'] = dict()
    if extended_structure.get('modules'):
        if not isinstance(extended_structure['modules'], dict):
            raise TypeError('modules should be stored as a dict')
    else:
        secop_errors.error_missing_mandatory('modules', 'node')
        return None
    for node_key in MANDATORY_NODE_KEYS:
        try:
            plain_structure[node_key] = extended_structure[node_key]
        except KeyError as err2_info:
            secop_errors.error_missing_mandatory(err2_info)
            return None
    for node_key in OPTIONAL_NODE_KEYS:
        try:
            plain_structure[node_key] = extended_structure[node_key]
        except KeyError:
            pass
    for module_name in extended_structure.get('modules'):
        current_module = extended_structure['modules'].get(module_name)
        new_module = dict()
        new_module['accessibles'] = dict()
        for module_key in MANDATORY_MODULE_KEYS:
            try:
                new_module[module_key] = current_module[module_key]
            except KeyError as err1_info:
                secop_errors.error_missing_mandatory(err1_info, 'module')
                return None
        for module_key in OPTIONAL_MODULE_KEYS:
            try:
                new_module[module_key] = current_module[module_key]
            except KeyError:
                pass
        for accessible_name in current_module.get('accessibles'):
            current_accessible = current_module['accessibles'].get(accessible_name)
            new_accessible = dict()
            for accessible_key in MANDATORY_ACCESSIBLE_KEYS:
                try:
                    new_accessible[accessible_key] = current_accessible[accessible_key]
                except KeyError as error_info:
                    secop_errors.error_missing_mandatory(error_info, module_name + ':' + accessible_name)
                    return None
            for accessible_key in OPTIONAL_ACCESSIBLE_KEYS:
                try:
                    new_accessible[accessible_key] = current_accessible[accessible_key]
                except KeyError:
                    pass
            try:
                new_module['accessibles'][accessible_name] = new_accessible
            except KeyError:
                pass
        plain_structure['modules'][module_name] = new_module
    return plain_structure


class Link(object):
    """ ____PROPERTIES____:

    gct ************************************* TO BE REMOVED *********************************
        gateway configuration topic.
        This topic can not be arbitrary. It defines how the gateway communicates.
        eg sending a link to hw/gw/socket/192.168.0.2/9999/add_link will start that gateway and apply the link.

        hw/gw/epics                         (/add_link) or (/remove_link)
        hw/gw/secop/<host>/<port>           (/add_link) or (/remove_link)
        hw/gw/socket/<host>/<port>          (/add_link) or (/remove_link)

        on the ECS-side whole extended-describe messages are needed. the topics are:
        ecs/gw/epics                        (/add_describe)
        ecs/gw/secop/port                   (/add_describe)


        The gct is the hardware specific part of the link, the rest is protocol specific.
    *************************************************************************************************

    value_topic (mandatory)
        the topic where the data for the accessible is published.
        If no get_topic is supplied /get will be appended automatically.
        If accessible is a writable(eg. it has a write reference) set_topic will be <value_topic>/set
        set, get and value can all be overridden by explicitly setting them: Never the less auto_topic HAS to be set
        because it is used as an identifier when link exists as json objects.

    get_topic
        the topic where you poll for data

    get_reference (mandatory for a HWGW)
        if the link is to a serial or other device that handles the payload via one channel it needs to be able to refer
        to that payload. get_reference is the reference for reading out data from the device.
        for Epics it will be the pv name, for a serial device the parameter name.

    set_topic
        the topic for issuing a command or setting a parameter.

    set_reference (mandatory for a writable HWGW)
        if the link is to a serial or other device that handles the payload via one channel it needs to be able to refer
        to that payload. set_reference is the reference for issuing a command or setting a parameter.
        for Epics it will be the pv name, for a serial device the parameter/command name.

    priority
        Priority is used by hardware gateways which has a limiting data transfer capacity(poll_intervall).
        0:  Highest priority, Should only be used by few, very important signals per device.
        1:  Default for writable, if the device is polled more often than pollintervall, writable signals should be
            prioritized.
        2:  Default for readables.

     """

    def __init__(self, ecs_link=False, **kwargs):
        keys_to_delete = list()
        for k, v in kwargs.items():
            if v == '':
                keys_to_delete.append(k)
        for k in keys_to_delete:
            del kwargs[k]
        self.recieved_dict = kwargs
        self.auto_topic = kwargs.get('auto_topic')
        self.gco = DEFAULT_GCO if not kwargs.get('gco') else kwargs.get('gco')
        self.description = '' if not kwargs.get('description') else kwargs.get('description')
        self.datainfo = None if not kwargs.get('datainfo') else datainfo.from_structure(kwargs.get('datainfo'))
        self.name = '' if not kwargs.get('name') else kwargs.get('name')
        self.readonly = kwargs.get('readonly')
        if self.readonly is None:  # Taking care of HW links.
            if self.auto_topic:
                to_be_deprecated('auto_topic')
                self.value_topic = self.auto_topic + '/value'\
                    if not kwargs.get('value_topic') else kwargs.get('value_topic')
                self.get_topic = self.auto_topic + '/get' if not kwargs.get('get_topic') else kwargs.get('get_topic')
                if kwargs.get('set_reference'):
                    self.set_topic = self.auto_topic + '/set'\
                        if not kwargs.get('set_topic') else kwargs.get('set_topic')
                else:
                    self.set_topic = None
            elif kwargs.get('value_topic'):
                self.value_topic = kwargs.get('value_topic')
                self.get_topic = self.value_topic + '/get' if not kwargs.get('get_topic') else kwargs.get('get_topic')
                if kwargs.get('set_reference'):
                    self.set_topic = self.value_topic + '/set'\
                        if not kwargs.get('set_topic') else kwargs.get('set_topic')
                else:
                    self.set_topic = None
                # self.value_topic = kwargs.get('value_topic')
                # self.get_topic = kwargs.get('get_topic')
                # self.set_topic = kwargs.get('set_topic')
            else:
                raise secop_errors.InternalError('atleast value_topic is needed')
            self.get_reference = kwargs.get('get_reference')
            self.set_reference = kwargs.get('set_reference')
        elif self.readonly is True:  # read_only is NOT none-> it is an ECS link.
            # read only is true -> it do not have a set reference.
            if self.auto_topic:
                self.value_topic = self.auto_topic + '/value'\
                    if not kwargs.get('value_topic') else kwargs.get('value_topic')
                self.get_topic = self.auto_topic + '/get'\
                    if not kwargs.get('get_topic') else kwargs.get('get_topic')
                self.get_reference = self.auto_topic + '/get'\
                    if not kwargs.get('get_reference') else kwargs.get('get_reference')
                self.set_topic = None
                self.set_reference = None
            else:
                self.value_topic = kwargs.get('value_topic')
                self.get_topic = kwargs.get('get_topic')
                self.get_reference = kwargs.get('get_reference')
        elif self.readonly is False:  # read_only is NOT none-> it is an ECS link.
            # read only is False -> it need a set reference.
            if self.auto_topic:
                self.value_topic = self.auto_topic + '/value'\
                    if not kwargs.get('value_topic') else kwargs.get('value_topic')
                self.get_topic = self.auto_topic + '/get'if not kwargs.get('get_topic') else kwargs.get('get_topic')
                self.set_topic = self.auto_topic + '/set' if not kwargs.get('set_topic') else kwargs.get('set_topic')
                self.get_reference = self.auto_topic + '/get'\
                    if not kwargs.get('get_reference') else kwargs.get('get_reference')
                self.set_reference = self.auto_topic + '/set'\
                    if not kwargs.get('set_reference') else kwargs.get('set_reference')
            else:
                self.value_topic = kwargs.get('value_topic')
                self.get_topic = kwargs.get('get_topic')
                self.set_topic = kwargs.get('set_topic')
                self.get_reference = kwargs.get('get_reference')
                self.set_reference = kwargs.get('set_reference')

        else:
            print("This is very strange")
        self.secop_datatype = kwargs.get('secop_datatype')
        self.gct = kwargs.get('gct')

        # TODO: AP test with adding static parameters to link object...(need to discuss)
        self.description = kwargs.get('description')
        self.get_reference_pv_obj = None
        self.set_reference_pv_obj = None
        self.last_value = None
        self.module = None

        # TODO: Adding extra parameter from links to set if HW/ECS, create PV, EPICS or TCPIP
        self.create_pv = kwargs.get('create_pv')
        self.ecs_hw = kwargs.get('ecs_hw')
        self.epics_tcpip = kwargs.get('epics_tcpip')
        # ATTENTION: Adding static parameters from accessible to th Hw links
        self.a_unit = kwargs.get('a_unit')
        self.accessible_datatype = kwargs.get('accessible_datatype')
        self.accessible_dict = kwargs.get('accessible')
        self.skipped = 0
        self.GW_datatype = None
        self.python_datatype = None
        self.buffer = []
        self.reply = None
        if kwargs.get('priority'):
            self.priority = kwargs.get('priority')
        elif ecs_link:
            self.priority = None
        else:
            try:
                if self.set_topic:
                    self.priority = 1
                else:
                    self.priority = 2
            except AttributeError as error_info:
                print(str(error_info))
        self.epics_reader = None
        self.epics_writer = None

    def __str__(self):
        return str(self.as_dict())

    def __eq__(self, other):
        """ Links are equal if they have the same value_topic"""
        return self.value_topic == other.value_topic

    def structure(self):
        return self.as_dict()

    def update_from_dict(self, update_dict):
        if isinstance(update_dict, dict):
            validated_dict = Link(**update_dict).as_dict()
            for key, value in validated_dict.items():
                setattr(self, key, value)
        else:
            raise TypeError(update_dict, 'is not a dict')

    def list_topics(self):
        """ List all topics in link"""
        ret = list()
        if self.value_topic:
            ret.append(self.value_topic)
        if self.set_topic:
            ret.append(self.set_topic)
        if self.get_topic:
            ret.append(self.get_topic)
        return ret

    def as_dict(self):
        ret = dict()
        if not self.auto_topic and not self.value_topic:
            raise KeyError('Either auto_topic or value_topic needs to be set')
        if self.auto_topic:
            ret['auto_topic'] = self.auto_topic
        if self.get_topic:
            ret['get_topic'] = self.get_topic
        if self.get_reference:
            ret['get_reference'] = self.get_reference
        if self.datainfo:
            ret['datainfo'] = self.datainfo.structure()
        try:
            if self.set_topic:
                ret['set_topic'] = self.set_topic
        except AttributeError as error_info:
            print(str(error_info))
        try:
            if self.set_reference:
                ret['set_reference'] = self.set_reference
        except AttributeError as error_info:
            print(str(error_info))
        if self.value_topic:
            ret['value_topic'] = self.value_topic
        if self.description:
            ret['description'] = self.description
        if self.secop_datatype:
            ret['secop_datatype'] = self.secop_datatype.structure()
        try:
            if self.priority:
                ret['priority'] = self.priority
        except AttributeError as error_info:
            print(str(error_info))
        #  TODO: AP testing to add static parameters...
        if self.description:
            ret['description'] = self.description
        if self.create_pv:
            ret['create_pv'] = self.create_pv
        if self.ecs_hw:
            ret['ecs_hw'] = self.ecs_hw
        if self.epics_tcpip:
            ret['epics_tcpip'] = self.epics_tcpip
        if self.a_unit:
            ret['a_unit'] = self.a_unit
        if self.accessible_datatype:
            ret['accessible_datatype'] = self.accessible_datatype
        if self.accessible_dict:
            ret['accessible'] = self.accessible_dict
        if self.readonly:
            ret['readonly'] = self.readonly

        #  TODO: This is a temporary test on how to keep some mandatory class variables and at the same time be able to
        #  just add "optional" variables to the link for development
        for key, value in self.recieved_dict.items():
            if key not in ret.keys():
                ret[key] = value
        return ret

    def as_json(self):
        return json.dumps(self.as_dict())

    def colonize(self):
        """ If PV name is created from the value/set topics, the separator need to be  replaced from EPICS naming"""
        if not self.get_reference:
            self.get_reference = self.value_topic
        self.get_reference = self.get_reference.replace('/', EPICS_PV_NAME_SEPARATOR)
        if self.set_reference:
            self.set_reference = self.set_reference.replace('/', EPICS_PV_NAME_SEPARATOR)

    def update_link(self, link):
        self.set_topic = link.set_topic
        self.get_topic = link.get_topic
        if link.description:
            self.description += "\n" + link.description


class LinkList(object):
    """ LinkList implement many of Python List functions and
    next_prioritized: for eg returning a prioritized link for a serial gateways where bandwidth is limiting
    find_by_<property> unambiguous searching for links depending on property
    """
    def __init__(self):
        self.links = dict()
        self.link_list = list(self.links.keys())
        self.next = 0
        self.last_priority = 0

    def __getitem__(self, item):
        try:
            return self.links[item]
        except KeyError as error_info:
            raise secop_errors.InternalError('Not a valid value topic: {}'.format(error_info))

    def __setitem__(self, key, value):
        self.links[key] = value

    def __str__(self):
        lst = []
        for link in self.links.values():
            lst.append(link)
        return str(lst)

    def __iter__(self):
        self.n = 0
        return self

    def delete(self, other):
        if isinstance(other, Link):
            del self.links[other.value_topic]
        elif isinstance(other, str):
            del self.links[other]
        self.link_list = list(self.links.keys())
        return self

    def add(self, other):
        if isinstance(other, Link):
            if other.value_topic not in self.links.keys():
                self.links[other.value_topic] = other
            else:
                self.links[other.value_topic].update_link(other)
        else:
            raise TypeError('object is not a Link')
        self.link_list = list(self.links.keys())
        return self

    def __next__(self):
        if self.n < len(self.link_list):
            ret = self.links[self.link_list[self.n]]
            self.n += 1
            return ret
        else:
            raise StopIteration

    def __len__(self):
        return len(self.links)

    def append(self, link):
        if isinstance(link, Link):
            self.add(link)
        else:
            raise TypeError('object is not a Link')

    def update(self, links):
        if isinstance(links, LinkList):
            self.links.update(links.links)
        elif isinstance(links, Link):
            self.add(links)
        else:
            raise TypeError('{} is not a Link or LinkList'.format(type(links)))

    def list_topics(self):
        """ List all topics in linklist"""
        ret = list()
        for link in self.links.values():
            ret.append(link.list_topics())
        return ret

    def next_prioritized(self):
        if len(self.link_list) == 0:
            raise secop_errors.InternalError('Cant use next_prioritized when LinkList is empty')
        for a in range(len(self.link_list)):
            self.next += 1
            if self.next >= len(self.link_list):
                self.next = 0
            current_link = self.links[self.link_list[self.next]]
            if current_link.get_reference:
                if current_link.skipped >= current_link.priority and current_link.priority + self.last_priority <= 4:
                    current_link.skipped = 0
                    self.last_priority = current_link.priority
                    return self.links[self.link_list[self.next]]
                else:
                    current_link.skipped += 1
                return self.next_prioritized()

    def find_by_get_reference(self, reference):
        reference = reference.strip()
        for link in self.links.values():
            if link.get_reference == reference:
                return link
        return None

    def find_by_set_reference(self, reference):
        reference = reference.strip()
        for link in self.links.values():
            if link.set_reference == reference:
                return link
        return None

    def find_by_auto_topic(self, auto_topic):
        to_be_deprecated('find_by auto_topic({})'.format(auto_topic))
        for link in self.links.values():
            if link.auto_topic == auto_topic:
                return link
        return None

    def find_by_value_topic(self, value_topic):
        if isinstance(value_topic, str):
            return self.links.get(value_topic)
        else:
            raise TypeError('object is not a value_topic')

    def find_by_set_topic(self, set_topic):

        for link in self.links.values():
            if link.set_topic == set_topic:
                return link
        return None

    def find_by_get_topic(self, get_topic):
        for link in self.links.values():
            if link.get_topic == get_topic:
                return link
        return None

    def add_link(self, link_dict):
        """Add a link to the Linklist. If the value_topic already exists in the link list do nothing and return None"""
        if isinstance(link_dict, dict):
            new_link = Link(**link_dict)
            self.append(new_link)
            return new_link
        else:
            raise TypeError('When adding a link it should be in form of a dict')

    def add_links_from_json_array(self, json_array):
        """Adds links from JSON array, returns list of link additions that was possible make"""
        lnks = []
        link_dict_list = json.loads(json_array)
        for link_dict in link_dict_list:
            added_link = self.add_link(link_dict)
            if added_link:
                lnks.append(added_link)
        return lnks

    def add_links_from_list(self, link_dict_list, colon=False):
        """Adds links from list, returns list of link additions that was possible make"""
        lnks = []
        for link_dict in link_dict_list:
            added_link = self.add_link(link_dict)
            if not added_link:
                print("Something is wrong, all links should be added. All previous stuff overwritten/removed")
            else:
                if colon:
                    print("colonizing")
                    added_link.colonize()
                if added_link:
                    lnks.append(added_link)
        return lnks

    def linklist_as_json(self):
        ret = list()
        for link in self.links.values():
            ret.append(link.as_dict())
        return json.dumps(ret)


if __name__ == '__main__':
    """Usage example"""
    some_data_string1 = 'SETP A,99.99'
    some_regex_string1 = r"(?P<reference>[\w\s]+),(?P<value>[\d\.]+)"

    """LinkList"""
    print('_'*5 + ' Linklists' + '_'*5)
    my_link_list = LinkList()
    my_link_list.add_links_from_json_array('[{"value_topic":"temp","get_reference":"IN_PV_00","set_topic":"setpoint",'
                                           '"set_reference":"OUT_SP_00"}]')
    my_link_list.add_links_from_json_array('[{"value_topic":"temp2","get_reference":"setpoint2"}]')
    my_link_list.add_links_from_json_array('[{"value_topic":"temp3","get_reference":"setpoint3", "priority": 4}]')
    my_link_list.add_links_from_json_array('[{"value_topic":"temp4","get_reference":"setpoint4"}]')
    my_link_list.add_links_from_json_array('[{"value_topic":"temp5","get_reference":"setpoint5","bajs":"kiss"}]')
    my_link_list2 = LinkList()
    bajs_lnk = my_link_list.find_by_get_reference('setpoint5')
    print('bajs link: {}'.format(bajs_lnk.value_topic))
    print('printing link for link:')
    for lnk in my_link_list:
        print(lnk)
    print('done printing link for link:')

    """Single Link"""
    print('_'*5 + ' Single Link ' + '_'*5)
    my_dict = {"value_topic": "temp19", "get_reference": "setpoint19", "datainfo": datainfo.suggest(25.6).structure()}
    my_link_from_dict = Link(**my_dict)
    print('Link from dict: {}'.format(my_link_from_dict))
    my_update_dict = {"value_topic": "temp20", "get_reference": "setpoint202", "set_reference": "tjohej"}
    my_link_from_dict.update_from_dict(my_update_dict)
    print('Updated Link from dict: {}'.format(my_link_from_dict))
    my_link = Link(value_topic="test1/hej/hopp", set_topic="setpoint5/tjosan/hejsan")
    print(my_link)
    print(my_link.set_topic)

    """Adding link"""
    print('_'*5 + ' Adding Link ' + '_'*5)
    my_link_list2.add(my_link)
    my_link_list2.add_links_from_json_array('[{"value_topic":"temp5","get_reference":"setpoint5"}, '
                                            '{"value_topic":"temp6","get_reference":"setpoint6"},'
                                            '{"value_topic":"temp7","get_reference":"setpoint7"},'
                                            '{"value_topic":"temp8","get_reference":"setpoint8"}]')

    """Combining LinkLists"""
    print('_'*5 + ' Combining LinkLists ' + '_'*5)
    my_link_list3 = LinkList()
    my_link_list3.update(my_link_list)
    my_link_list3.update(my_link_list2)
    print(my_link_list3.linklist_as_json())

    """Comparing LinkLists"""
    print('_'*5 + ' Comparing LinkLists ' + '_'*5)
    print('LinkLists are the same? {}'.format(my_link_list == my_link_list2))

    my_link_list.add_links_from_json_array('[{"value_topic":"test1/hejsan/hoppsan"}]')

    """Appending Link"""
    print('_'*5 + ' Appending Link ' + '_'*5)
    my_link_list.append(Link(value_topic="test1/hej/hopp2", set_topic="setpoint5/tjosan/hejsan2",
                             description='link to append'))

    """Appending duplicate Link"""
    my_link_list.append(Link(value_topic="test1/hej/hopp2", set_topic="setpoint5/tjosan/hejsan2",
                             description='appended same link again'))

    """Indexing LinkList"""
    print('_'*5 + ' Indexing LinkLists ' + '_'*5)
    print("link 2")
    try:
        print(my_link_list[2])
    except secop_errors.InternalError as err_info:
        print('SEcop_error: {}'.format(err_info))

    """Find and remove Link from LinkList"""
    print('_'*5 + ' Find and remove Link from LinkList ' + '_'*5)

    print(my_link_list)
    print('the link: {}'.format(my_link_list.find_by_value_topic('temp3')))
    my_link_list.update(my_link_list.find_by_value_topic('temp3'))

    print(my_link_list.find_by_value_topic('test1/hejsan/hopp2'))

    iot_link = Link(value_topic='test_value', set_topic='test_set', description='This is a test for IOT hardware')
    print(iot_link)
    print(json.dumps(my_link_list.list_topics()))
    print('as_json:')
    print(my_link_list.linklist_as_json())
    print(my_link_list.find_by_value_topic('test1/hej/hopp2').description)

    print("Next_prioritized, polling hardware 20 times depending on priority:")
    for n in range(20):
        lnk = my_link_list.next_prioritized()
        print('Sending command: {} with priority: {} to hardware'.format(lnk.get_reference, lnk.priority))
        if not lnk.get_reference:
            print(lnk)
#    test_desc = r'{"equipment_id":"mockup1","description":"mockup for some signals","modules":{"dummyjulabo":{"description":"A mock module for adding some signals from a julabo.\n\nvalue has only a HW_link, Target has different value_topics","accessibles":{"value":{"description":"The current temperature for the water bath","datainfo":{"type":"double","min":0,"max":100,"unit":"C"},"HW_link":{"value_topic":"julabo/Tctrl","get_topic":"julabo/Tctrl/get","get_reference":"IN_PV_00","priority":0}},"target":{"description":"The target temperature for the water bath","datainfo":{"type":"double","min":0,"max":100,"unit":"C"},"ECS_link":{"value_topic ":"julabo/Tctrl/target","get_topic ":"julabo/Tctrl/target/get","set_topic":"julabo/Tctrl/target/set","get_reference":"julabo1:Tctrl:target:get","set_reference":"julabo1:Tctrl:target:set"},"HW_link":{"value_topic":"julabo/target","set_reference":"OUT_SP_00","get_reference":"IN_SP_00","priority":1}},"circulation":{"datainfo":{"type":"bool","members":{"OFF":0,"ON":1}},"description":"start/stop the circulation and controller","ECS_link":{"get_reference":"julabo1:Tctrl:circulation","set_reference":"julabo1:Tctrl:circulation:set"},"HW_link":{"value_topic":"julabo/mode","get_reference":"IN_MODE_05","set_reference":"OUT_MODE_05","priority":0}, "readonly": false}}}}}'
#    test_desc_anders = r'{"equipment_id":"mockup for some signals","modules":{"dummyjulabo":{"description":"a mock module for adding some signals from a julabo","accessibles":{"value":{"datainfo":{"type":"double","min":0,"max":100,"unit":"C"},"ECS_link":{"value_topic":"julabo1/Tctrl","datatype":"double","epics":true,"priority":2}},"target":{"datainfo":{"type":"double","min":0,"max":100,"unit":"C"},"ECS_link":{"get_reference":"julabo1:Tctrl:target","set_reference":"julabo1:Tctrl:target:set","value_topic":"julabo1/Tctrl/target","datatype":"double","priority":1,"create_pv":true}},"circulation":{"datainfo":{"type":"bool","members":{"OFF":0,"ON":1}},"description":"start/stop the circulation and controller","ECS_link":{"value_topic":"julabo1/Tctrl/circulation","get_reference":"julabo1:Tctrl:circulation","set_reference":"julabo1:Tctrl:circulation:set","datatype":"int","priority":1}}}}}}'
    kepco_desc = r'{"equipment_id":"kepco_001","description":"test","modules":{"volt_ctrl":{"description":"Module for volt control","accessibles":{"value":{"description":"test","datainfo":{"type":"double","min":-50,"max":50,"unit":"V"},"ECS_link":{"auto_topic":"Kepco_001/Vctrl","get_topic":"","get_reference":"","value_topic":"","datatype":"double","epics":true,"priority":2}},"target":{"description":"test","datainfo":{"type":"double","min":-50,"max":50,"unit":"V"},"ECS_link":{"auto_topic":"","get_topic":"","get_reference":"","set_topic":"","set_reference":"kepco_001:Vctrl:target:set","value_topic":"kepco_001/Vctrl/target","datatype":"double","priority":1,"create_pv":true}}}},"curr_ctrl":{"description":"Module for current control","accessibles":{"value":{"description":"test","datainfo":{"type":"double","min":-50,"max":50,"unit":"C"},"ECS_link":{"auto_topic":"Kepco_001/Cctrl","get_topic":"","get_reference":"","value_topic":"","datatype":"double","epics":true,"priority":2}},"target":{"description":"test","datainfo":{"type":"double","min":-50,"max":50,"unit":"C"},"ECS_link":{"auto_topic":"","get_topic":"","get_reference":"","set_topic":"","set_reference":"kepco_001:Cctrl:target:set","value_topic":"kepco_001/Cctrl/target","datatype":"double","priority":1,"create_pv":true}}}},"settings":{"description":"Module for selecting operation mode etc","accessibles":{"value":{"description":"test","datainfo":{"type":"bool","members":{"VOLT":0,"CURR":1}},"ECS_link":{"auto_topic":"Kepco_001/settings/mode","get_topic":"","get_reference":"","value_topic":"","datatype":"enum","epics":true,"priority":2}},"mode":{"description":"test","datainfo":{"type":"bool","members":{"VOLT":0,"CURR":1}},"ECS_link":{"auto_topic":"Kepco_001/settings/mode","get_topic":"","get_reference":"","set_topic":"","set_reference":"","value_topic":"Kepco_001/settings/mode","datatype":"double","priority":1,"create_pv":true}},"output":{"description":"test","datainfo":{"type":"bool","members":{"OFF":0,"ON":1}},"ECS_link":{"auto_topic":"Kepco_001/settings/output","get_topic":"","get_reference":"","set_topic":"","set_reference":"","value_topic":"Kepco_001/settings/output","datatype":"bool","priority":1,"create_pv":true}},"id":{"description":"test","datainfo":{"type":"string","maxchars":60},"ECS_link":{"auto_topic":"Kepco_001/settings/id","get_topic":"","get_reference":"","set_topic":"","set_reference":"","value_topic":"Kepco_001/settings/id","datatype":"string","priority":1,"create_pv":true}},"remote":{"description":"test","datainfo":{"type":"bool","members":{"OFF":0,"ON":1}},"ECS_link":{"auto_topic":"Kepco_001/settings/remote","get_topic":"","get_reference":"","set_topic":"","set_reference":"","value_topic":"Kepco_001/settings/remote","datatype":"bool","priority":1,"create_pv":true}}}}}}'
#    test_desc_linklist = make_ecs_link_list(json.loads(test_desc))
#    test_desc_linklist_anders = make_ecs_link_list(json.loads(test_desc_anders))
    kepco_desc_linklist = make_ecs_link_list(json.loads(kepco_desc))
#    for lnk in test_desc_linklist:
#        print(lnk)
    for lnk in kepco_desc_linklist:
        print(lnk)
#    print(json.dumps(validate_structure(json.loads(test_desc)), indent=4))
    ne_test = Link(value_topic='test', set_reference='test_set', get_reference='test_get')
    print(ne_test)
    anders_test_json = '{"description":"A powersupply able to either control current or voltage","equipment_id":"kepco_001","modules":{"volt_ctrl":{"description":"Module for volt control","accessibles":{"value":{"description":"Measured voltage","datainfo":{"type":"double","min":-50,"max":50,"unit":"V"},"link":{"auto_topic":"kepco_001/Vctrl/test/value","get_topic":"","get_reference":"","value_topic":"","datatype":"double","epics":true,"priority":2}},"target":{"description":"Target for voltage","datainfo":{"type":"double","min":-50,"max":50,"unit":"V"},"ECS_link":{"auto_topic":"","get_topic":"","get_reference":"","set_topic":"","set_reference":"kepco_001:Vctrl:target:set","value_topic":"kepco_001/Vctrl/target","datatype":"double","priority":1,"create_pv":true}}}},"curr_ctrl":{"description":"Module for current control","accessibles":{"value":{"description":"Measured current","datainfo":{"type":"double","min":-50,"max":50,"unit":"C"},"ECS_link":{"value_topic":"kepco_001/Cctrl","set_topic":"hge","datatype":"double","epics":true,"priority":2}},"target":{"description":"Target value for current","datainfo":{"type":"double","min":-50,"max":50,"unit":"C"},"ECS_link":{"auto_topic":"","get_topic":"","get_reference":"","set_topic":"","set_reference":"kepco_001:Cctrl:target:set","value_topic":"kepco_001/Cctrl/target","datatype":"double","priority":1,"create_pv":true}}}},"settings":{"description":"Module for selecting operation mode, output ON/OFF etc","accessibles":{"value":{"description":"Current control mode of the module","datainfo":{"type":"bool","members":{"VOLT":0,"CURR":1}},"ECS_link":{"auto_topic":"kepco_001/settings/mode","get_topic":"","get_reference":"","value_topic":"","datatype":"enum","epics":true,"priority":2}},"mode":{"description":"Select Voltage or Current control","datainfo":{"type":"enum","members":{"VOLT":0,"CURR":1}},"ECS_link":{"auto_topic":"kepco_001/settings/mode","get_topic":"","get_reference":"","set_topic":"","set_reference":"kepco_001:settings:mode:set","value_topic":"kepco_001/settings/mode","datatype":"","priority":1,"create_pv":true}},"output":{"description":"Activates the output ON/OFF","datainfo":{"type":"bool","members":{"OFF":0,"ON":1}},"ECS_link":{"auto_topic":"kepco_001/settings/output","get_topic":"","get_reference":"","set_topic":"","set_reference":"kepco_001:settings:output:set","value_topic":"kepco_001/settings/output","datatype":"bool","priority":1,"create_pv":true}},"id":{"description":"Hardware identification","datainfo":{"type":"string","maxchars":60},"ECS_link":{"auto_topic":"kepco_001/settings/id","get_topic":"","get_reference":"","set_topic":"","set_reference":"","value_topic":"kepco_001/settings/id","datatype":"string","priority":1,"create_pv":true}},"remote":{"description":"Switching between remote and local control","datainfo":{"type":"enum","members":{"OFF":0,"ON":1}},"ECS_link":{"auto_topic":"kepco_001/settings/remote","get_topic":"","get_reference":"","set_topic":"","set_reference":"kepco_001:settings:remote:set","value_topic":"kepco_001/settings/remote","datatype":"","priority":1,"create_pv":true}}}}}}'
    print('Printing Anders stuff:')
#    lnk_lst_anders = make_ecs_link_list(json.loads(anders_test_json))
#    for lnk_a in lnk_lst_anders:
 #       print(lnk_a)
#    print(json.dumps(validate_structure(json.loads(anders_test_json)), indent=4))
