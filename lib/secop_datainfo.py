import re
import math
import lib.secop_errors as errors
import json
FMT_STR = re.compile('^%[.][0-9]+')
NBR_STR = re.compile('[^0-9/.]')


def from_structure(structure):
    """Returns datainfo class from datainfo part of structure report"""
    if structure:
        data_info_type_name = structure.get('type')
        if data_info_type_name is None:
            raise errors.BadJSON('datainfo: missing type in: ' + str(structure))
        data_info_class_name = types.get(data_info_type_name)
        return data_info_class_name(**structure)
    else:
        return None


def suggest(data, parse_from_string=False):
    """Analyzing data to find out how it could fit into SECoP data types and returning the suggested class"""
    if isinstance(data, int):
        return SecInt()
    if isinstance(data, float):
        return SecFloat()
    if isinstance(data, dict):
        members = dict()
        for k, v in data.items():
            members[k] = suggest(v).structure()
        return SecStruct(members=members)
    if isinstance(data, list):
        return SecArray(members=suggest(data[0]).structure(), max=len(data))
    if isinstance(data, str):
        if not parse_from_string:
            return SecString()
        if NBR_STR.match(data):
            decimals = data.count('.')
            if decimals == 1:
                return SecFloat
            elif decimals == 0:
                return SecInt
            else:
                return SecString


class DataInfo(object):
    def __init__(self, **kwargs):
        self.min = kwargs.get('min')
        self.max = kwargs.get('max')
        self._inner_type_fnk = None
        self._value = None

    def structure(self):
        ret = {k: v for k, v in self.__dict__.items() if v is not None and not k.startswith('_')}
        if 'members' in ret.keys():
            members = ret.get('members')
            # if not isinstance(members, dict):
            #     ret['members'] = members.structure()
            if isinstance(members, list):
                member_list = list()
                for member in members:
                    if isinstance(member, DataInfo):
                        structure = member.structure()
                        member_list.append(structure)
                ret['members'] = member_list
        elif 'argument' in ret.keys():
            ret['argument'] = ret.get('argument').structure()
            ret['result'] = ret.get('result').structure()
        return ret

    def __str__(self):
        return str(self.structure())

    def validate(self, val, set_value=False):
        pass


class SecFloat(DataInfo):
    def __init__(self, **kwargs):
        self.type = 'double'
        super(SecFloat, self).__init__(**kwargs)

    def validate(self, val, set_value=False):
        try:
            val = float(val)
        except ValueError as err:
            raise errors.WrongTypeError('secop_data_type ' + self.type + ' validation error: ' + str(err))
        if self.max and set_value:
            if val > self.max:
                raise errors.RangeError(str(val) + ' exceeds the maximum limit of ' + str(self.max))
        if self.min and set_value:
            if val < self.min:
                raise errors.RangeError(str(val) + ' under the lower limit of ' + str(self.min))
        return val


class SecInt(DataInfo):
    def __init__(self, **kwargs):
        self.type = 'int'
        super(SecInt, self).__init__(**kwargs)

    def validate(self, val, set_value=False):
        try:
            val = int(val)
            if self.min and val < self.min and set_value:
                raise errors.RangeError(
                    str(val) + ' should be an int between ' + str(self.min) + ' and ' + str(self.max))
            if self.max and val > self.max and set_value:
                raise errors.RangeError(
                    str(val) + ' should be an int between ' + str(self.min) + ' and ' + str(self.max))
        except ValueError as err:
            raise errors.WrongTypeError('secop_data_type ' + self.type + ' validation error: ' + str(err))
        except TypeError as err:
            raise errors.WrongTypeError('secop_data_type ' + self.type + ' validation error: ' + str(err))
        return val


class SecScaled(DataInfo):
    def __init__(self, **kwargs):
        self.type = 'scaled'
        scale = kwargs.get('scale_factor')
        if not scale:
            print('Warning: No valid scale_factor, setting to 1')
            scale = 1
        try:
            self.scale = float(scale)
        except ValueError as err:
            print(str(err))
        except TypeError as err:
            raise TypeError('Error: No valid scale_factor, ' + str(err))
        self.fmtstr = str(kwargs.get('fmtstr'))
        if not self.fmtstr or not FMT_STR.match(self.fmtstr):
            self.fmtstr = f'%.{max(0, -math.floor(math.log10(self.scale)))}f'
        super(SecScaled, self).__init__(**kwargs)

    def validate(self, val, set_value=False):
        try:
            val = int(float(val)*self.scale)
            if self.min and val < self.min and set_value:
                raise errors.RangeError(
                    str(val) + ' should be an int between ' + str(self.min) + ' and ' +
                    str(self.max))
            if self.max and val > self.max and set_value:
                raise errors.RangeError(
                    str(val) + ' should be an int between ' + str(self.min) + ' and ' +
                    str(self.max))
        except ValueError as err:
            raise errors.WrongTypeError('secop_data_type ' + self.type + ' validation error: ' + str(err))
        return val

    def to_string(self, val):
        '{' + self.fmtstr.format(self.validate(val))


class SecBool(DataInfo):
    def __init__(self, **kwargs):
        self.type = 'bool'
        super(SecBool, self).__init__(**kwargs)

    def validate(self, val, set_value=False):
        try:
            val = bool(val)
        except ValueError as err:
            raise errors.WrongTypeError('secop_data_type ' + self.type + ' validation error: ' + str(err))
        return val


class SecString(DataInfo):
    def __init__(self, **kwargs):
        self.type = 'string'
        super(SecString, self).__init__(**kwargs)

    def validate(self, val, set_value=False):
        try:
            ret = str(val)
        except ValueError as err:
            raise errors.WrongTypeError(str(err))
        return ret


class SecArray(DataInfo):
    def __init__(self, **kwargs):
        self.type = 'array'
        self._py_types = (tuple, list)
        self._inner_type_fnk = types.get(kwargs.get('members').get('type'))
        self._inner_type = self._inner_type_fnk(**kwargs.get('members'))
        self.members = kwargs.get('members') #  self._inner_type
        if not isinstance(self._inner_type, DataInfo):
            raise ValueError('SecArray can only contain Sec data types')
        self.min = int(kwargs.get('min')) if kwargs.get('min') else 0
        if not kwargs.get('max'):
            raise ValueError('SecArray must have a maximum length')
        self.max = int(kwargs.get('max'))
        super(SecArray, self).__init__(**kwargs)

    def add_member(self, member):
        if isinstance(member, type(self._inner_type)):
            self.max += 1
            # self.members.update(member.structure())
        else:
            raise errors.WrongTypeError('For SecArray all members has to be the same type')

    def validate(self, val, set_value=False):
        ret = []
        if isinstance(val, self._py_types):
            if self.min and set_value:
                if len(val) < self.min and self.min:
                    raise errors.RangeError('Array length less than the specified ' + str(self.min) +
                                            ' for this SecArray')
            if len(val) > self.max and self.min and set_value:
                raise errors.RangeError('Array length more than the specified ' + str(self.max) +
                                        ' for this SecArray')
            for element in val:
                ret.append(self._inner_type.validate(element))
            return ret
        else:
            raise errors.WrongTypeError('secop_data_type ' + self.type + ' validation error')


class SecStruct(DataInfo):
    def __init__(self, **kwargs):
        self.type = 'struct'
        self._inner_types = {}
        self._py_types = dict
        self.members = dict() if not kwargs.get('members') else kwargs.get('members')
        if self.members:
            for member in self.members:
                sec_type = self.members.get(member)
                if sec_type.get('type') not in types.keys():
                    raise ValueError(sec_type.get('type') + ' is not a valid type')
                self._inner_types[member] = {'func': types.get(sec_type.get('type'))}
                self._inner_types.get(member)['inner_type'] = self._inner_types.get(member).get('func')(**sec_type)
            if not self._inner_types:
                raise ValueError('Struct members has to be defined')
        super(SecStruct, self).__init__(**kwargs)

    def add_member(self, key, member):
        if issubclass(type(member), DataInfo):
            if isinstance(key, str):
                self.members.update({key: member.structure()})
            else:
                raise errors.WrongTypeError('A SecStruct member has to have a key')
        else:
            raise errors.WrongTypeError('Data type has to belong to the DataInfo class')

    def validate(self, val, set_value=False):
        ret = {}
        if isinstance(val, self._py_types):
            for key in val.keys():
                if key in self._inner_types.keys():
                    ret[key] = self._inner_types.get(key).get('inner_type').validate(val.get(key), set_value)
        else:
            raise errors.WrongTypeError('secop_data_type ' + self.type + ' validation error: ')
        return ret


class SecEnum(DataInfo):
    def __init__(self, **kwargs):
        self.type = 'enum'
        self.members = kwargs.get('members')
        self._py_types = dict
        if not isinstance(self.members, dict):
            raise ValueError('Enum members has to be defined')
        super(SecEnum, self).__init__(**kwargs)

    def validate(self, val, set_value=False):
        try:
            ret = int(val)
        except ValueError as err:
            raise errors.WrongTypeError(str(err))
        if ret not in self.members.values() and set_value:
            raise errors.WrongTypeError('secop_data_type ' + self.type + ' validation error: Number ' + str(ret) +
                                        ' is not represented in enum description')
        return ret


class SecTuple(DataInfo):
    def __init__(self, **kwargs):
        self.type = 'tuple'
        self._py_types = (tuple, list)
        self.members = list()
        self.members = kwargs.get('members')
        self._inner_types = list()
        for member in self.members:
            self._inner_type_fnk = types.get(member.get('type'))
            inner_type = self._inner_type_fnk(**member)
            if not isinstance(inner_type, DataInfo):
                raise ValueError('SecTuple can only contain Sec data types')
            self._inner_types.append(inner_type)
        self.members = self._inner_types
        super(SecTuple, self).__init__(**kwargs)

    def validate(self, val, set_value=False):
        ret = list()
        nbr_of_values_in = len(val)
        if isinstance(val, self._py_types):
            if nbr_of_values_in < len(self.members) and set_value:
                raise errors.RangeError('Tuple length less than the specified ' + str(len(self.members)) +
                                        ' for this SecTuple')
            if nbr_of_values_in > len(self.members) and set_value:
                raise errors.RangeError('Array length more than the specified ' + str(len(self.members)) +
                                        ' for this SecArray')
            nbr = 0
            for element in val:
                ret.append(self._inner_types[nbr].validate(element, set_value))
                nbr += 1
            return ret
        else:
            raise errors.WrongTypeError('secop_data_type ' + self.type + ' validation error')


class SecCommand(DataInfo):
    def __init__(self, **kwargs):
        self.type = 'command'
        self.argument = from_structure(kwargs.get('argument'))
        self.result = from_structure(kwargs.get('result'))
        # if not self.argument or not self.result:
        #     raise errors.ProtocolErrorError('Not a valid command datainfo structure')
        super(SecCommand, self).__init__(**kwargs)

    def validate(self, val, set_value=False):
        raise ValueError('not possible to validate a command, only argument or result')


types = {'double': SecFloat,
         'int': SecInt,
         'bool': SecBool,
         'scaled': SecScaled,
         'array': SecArray,
         'string': SecString,
         'struct': SecStruct,
         'enum': SecEnum,
         'tuple': SecTuple,
         'command': SecCommand
         }



if __name__ == "__main__":
    b = 1234
    b_ = suggest(b)
    a = 'test'
    print(suggest(a))
    array = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    my_array = suggest(array)
    my_array.add_member(b_)
#    my_array.members.update({'type': 'string'})
    print('validated array: ')
    print(my_array.validate(array))
    print(my_array)
    struct = {"int": 45546, "double": 341.65999999983427, "string": "test"}
    my_struct = suggest(struct)
    print(my_struct)
    print(my_struct.validate(struct))

    to_add = suggest(1234)
    print(to_add)
    my_struct.add_member('a new int', to_add)
    print(my_struct)
    advanced = json.loads('{"int":15035,"double":112.8000000000208,"string":"test","array":[1,2,3,4,5,6,15035]}')
    my_advanced = suggest(advanced)
    print(my_advanced)

#    sf = SecFloat(min=0.05, max=99.95)
#    si = SecInt(min=0, max=100)
#    sa = SecArray(type='array', members={'type': 'array', 'members': {'type': 'int', 'min_len': 1, 'max_len': 200}, 'max': 7}, max_len=10)
#    int_test_str = '{"type": "array", "members": {"type": "array", "members": {"type": "int", "min": 1, "max": 200}, "max_len":8}, "max_len":7}'
#     tuple_test_str = '{"type":"tuple","members":[{"type":"enum","members":{"DISABLED":0,"IDLE":100,"WARN":200,"UNSTABLE":270,"BUSY":300,"ERROR":400}},{"type":"string"}]}'
#    scaled_test_str = '{"type": "array", "members": {"type": "array", "members": {"type": "scaled", "min": 1, "max": 200, "scale_factor" : 0.1}, "max_len":8}, "max_len":7}'
    enum_test_str = '{"type":"enum","members":{"OFF":0,"ON":1}}'
    struct_test = '{"type": "struct", "members": {"a": {"type" : "int", "min": 1, "max": 200}}}'
#     struct_test = '{"type":"struct","test":"test","members":{"x":{"type":"int","min":1,"max":200},"y":{"type":"array","members":{"type":"array","members":{"type":"scaled","min":0,"max":200,"scale_factor":1},"min_len":2,"max_len":8},"min_len":3,"max_len":7}}}'
#    st = SecStruct(**json.loads(struct_test))
#    sa2 = SecArray(**json.loads(int_test_str))
#    ss = SecArray(**json.loads(scaled_test_str))
    se = SecEnum(**json.loads(enum_test_str))
    print(se.structure())

#    print('struct: ' + str(secop_pair_array(**st.structure())))

 #   st_structure = st.structure()
#    print(type(st_structure))
 #   print('st structure: ' + str(st))

#    obj_str = json.loads(struct_test)
 #   print('spa struct: ' + json.dumps(secop_pair_array(dict(json.loads(struct_test)))))
#    print(json.dumps(sf.structure()))
#    print(si)
#    print(json.dumps(si.structure()))
#    print(sa)
#    print(sa2)
#    out = sa.validate(json.loads('[[1,201],[2,3],[4],[3,4,5,6.8]]'))
#    print(out)
#    out = ss.validate(json.loads('[[1,201],[2,3],[4],[3,4,5,6.8]]'))
 #   print(ss)
 #   print(json.dumps(ss.structure()))
#    print(ss)
#     double_json = '{"type":"double","min":-100,"max":200,"unit":"C"}'
#     print('+' * 20)
#     sd = from_structure(json.loads(double_json))
#     print('-'*20)
#     print(sd)
#     print(json.dumps(sd.structure()))

    # out = st.validate(json.loads('{"x": 100, "y": [[1,200],[2,3],[4,5],[3,4,5,6.8]]}'))
    # out = sd.validate('34.56')
    # print('validated double ' + str(out))
    # print(st)
    # print(se.validate(0))
    # my_int = SecInt(min=0, max=100)
    # my_int.value = 10
    # print(my_int)
    # print(my_int.validate('55'))
#     print(json.dumps(my_int.structure()))
#
#     stup = SecTuple(**json.loads(tuple_test_str))
#     stup_test = json.loads('[100, "hej hopp"]')
#     print(stup_test)
#     print(stup.validate(stup_test))
#     print(stup)
#     print(st)

    # command_test_string = '{"argument":{"type":"enum","members":{"Off":0,"On":1}},"result":{"type":"enum","members":{"Off":0,"On":1}},"type":"command"}'
    # test = json.loads(command_test_string)
    # print(type(test))
    # ct = from_structure(test)
    # print(ct.argument.validate(1))
    # print(ct)
    # print(json.dumps(str(ct)))

